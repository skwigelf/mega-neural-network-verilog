module multiplier 
	#(parameter MATRIX_SIZE = 5, parameter NUMBER_SIZE = 15)(
	input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0],
    input wire [NUMBER_SIZE:0] kernel [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0],
    output wire [NUMBER_SIZE:0] conv);
    
    wire [NUMBER_SIZE:0] buff_res[MATRIX_SIZE * MATRIX_SIZE - 1:0];
	wire [NUMBER_SIZE:0] buff[MATRIX_SIZE * MATRIX_SIZE - 1:0]; 	
	
	reg [4:0] counter = 0;
	
	
	wire sub = 0;
	genvar col;
	genvar row;
	generate
		begin
			for (row=0; row < MATRIX_SIZE; row=row+1)
				begin: mplyGen
					for (col=0; col < MATRIX_SIZE; col=col+1)
					begin: mplyGen1_1
						fmpy_p fmpy_p(.D1(matrix[row][col]),.D2(kernel[row][col]),.DW(buff_res[row * (MATRIX_SIZE) + col]));
					end
				end	
		end	
	endgenerate	  				  
	assign buff[0] = buff_res[0];	
	genvar i;
	generate
		begin
			for (i=1; i < (MATRIX_SIZE) * (MATRIX_SIZE); i=i+1)
				begin: mplyGen2	
						fadd0p fadd0p(.D0(buff[i - 1]),.D1(buff_res[i]),.SUB(sub),.DW(buff[i])); 
				end
		end	
		
	endgenerate	 
	ReLu ReLu(.value(buff[MATRIX_SIZE * MATRIX_SIZE - 1]), .result(conv));
endmodule