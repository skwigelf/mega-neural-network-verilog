module fp_comparator #(parameter NUMBER_SIZE = 15) (
	input [NUMBER_SIZE:0] first_val,
	input [NUMBER_SIZE:0] sec_val,
	output reg [NUMBER_SIZE:0] result);
	
	//0 - less. 1 - equal, 2 more
	always_comb
	begin
		if (first_val[0] == 0)
			begin
				if (sec_val[0] == 1)
					result = first_val;
				else if (first_val > sec_val)
					result = first_val;
				else if (first_val == sec_val)
					result = sec_val;
				else 
					result = sec_val;
			end
		else 
			begin
				if (sec_val[0] == 0)
					result = sec_val;
				else if (first_val < sec_val)
					result = first_val;
				else if (first_val == sec_val)
					result = sec_val;
				else 
					result = sec_val;
			end
	end
endmodule