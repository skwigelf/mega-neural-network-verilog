module submatrix_flat 
	#(parameter MATRIX_SIZE = 6,
	parameter NUMBER_SIZE = 15,
	parameter FEATURE_MAPS = 5,
	parameter RESULT_SIZE = 2)
	(
		input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE-1:0],
		input reg [4:0] row_index,	
		input wire [4:0] column_index,	   
		output wire [NUMBER_SIZE:0] conv [RESULT_SIZE - 1:0]
	);		 
	
	genvar i;
	generate 
		begin
		for (i = 0; i < RESULT_SIZE; i = i + 1)
			begin : rowExtractor
				assign conv[i] = matrix[row_index][column_index + i];
			end
		end
	endgenerate
	
endmodule