module dense_layer 
	#(parameter NUMBER_SIZE = 15,
	  parameter MATRIX_SIZE = 6,
	  parameter FEATURE_MAPS = 5,
	  parameter CLASS_NUMBERS = 10,
	  parameter ROM_PACKAGE = 12,
	  parameter FLAT_MATRIX_SIZE = 2)(
	input [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0], 
	//input [NUMBER_SIZE:0] weights [CLASS_NUMBERS - 1:0][MATRIX_SIZE * MATRIX_SIZE * FEATURE_MAPS - 1:0], 
	input clk,
	input start,
	output wire finished,
	output wire [NUMBER_SIZE:0] result [CLASS_NUMBERS - 1:0]
);

wire [NUMBER_SIZE:0] array [MATRIX_SIZE * MATRIX_SIZE - 1:0];

reg [NUMBER_SIZE:0] neurons_sum [CLASS_NUMBERS:0];

reg [NUMBER_SIZE:0] temp_flat_matrix [FLAT_MATRIX_SIZE - 1:0];
//reg [NUMBER_SIZE:0] temp_flat_matrix_reg [1:0];

reg [NUMBER_SIZE:0] temp_flat_weights [FLAT_MATRIX_SIZE - 1:0];

wire [NUMBER_SIZE:0] buffer_weights [ROM_PACKAGE - 1:0];
wire [NUMBER_SIZE * ROM_PACKAGE - 1:0] buffer_weights_flat;
//reg [NUMBER_SIZE:0] temp_flat_weights_reg [1:0];

reg [NUMBER_SIZE:0] res_reg [CLASS_NUMBERS - 1:0];

reg [NUMBER_SIZE:0] result_sum;

wire [NUMBER_SIZE:0] multiplier_result_wire [FLAT_MATRIX_SIZE - 1:0];
reg [NUMBER_SIZE:0] multiplier_result [FLAT_MATRIX_SIZE - 1:0];

wire [NUMBER_SIZE:0] result_neurons_2;
reg [NUMBER_SIZE:0] accumulator = 0;

wire sub = 0;
reg [5:0] col = 0;
reg [5:0] row = 0;
reg [4:0] classes = 0;

reg [8:0] weight_pointer = 0;
reg [3:0] weight_offset = 0;

reg weight_loading = 1;

reg calculating_finished = 0;

always@(posedge clk)
	begin	
	if (!start)
		begin
			col = 0;
			classes = 0;
			finished = 0;
		end 
	else
		begin
			if (weight_loading)
				begin
					weight_loading = 0;	
					weight_offset = 0;
				end
			else
			begin
				if (!calculating_finished)
					begin
						calculating_finished = 1;
						multiplier_result = multiplier_result_wire;
					end
				else
				begin
					if (weight_offset == ROM_PACKAGE - FLAT_MATRIX_SIZE)
						begin
							weight_pointer = weight_pointer + 1;
							
							weight_loading = 1;
						end
					else 
						begin
							weight_offset = weight_offset + FLAT_MATRIX_SIZE;
						end
					if (classes == CLASS_NUMBERS)
						begin
							finished = 1;
						end
					if (!finished)
						begin	 
							calculating_finished = 0;
							accumulator = result_sum;
							if (col < MATRIX_SIZE * MATRIX_SIZE - FLAT_MATRIX_SIZE)
								col = col + FLAT_MATRIX_SIZE;
							else 
								begin
									res_reg[classes] = accumulator;	
									accumulator = 0;
									col = 0;
									classes = classes + 1;
								end		 
						end
				end
				
			end
		end
	end
	
	rom_dense rom_dense(.address(weight_pointer), .clock(clk), .q(buffer_weights_flat));
	flat_to_array_160 flat_to_array_160(.value(buffer_weights_flat), .matrix(buffer_weights));
	
	matrix_to_flat_array matrix_to_flat_array(.matrix(matrix), .array(array));

	subvector_flat subvector_flat(.array(array), .column_index(col), .conv(temp_flat_matrix));
	submatrix_flat_144 submatrix_flat_144(.matrix(buffer_weights), .column_index(weight_offset), .conv(temp_flat_weights));
	multiplier_row_only_2 multiplier_row_only_2(
	.vector(temp_flat_matrix),
	.weights(temp_flat_weights), 
	.res(multiplier_result_wire));
	adder_rows_2 adder_rows_2(.vector(multiplier_result), .res(result_neurons_2));
	fadd0p fadd0p(.D0(result_neurons_2), .D1(accumulator), .SUB(sub), .DW(result_sum));
	
	assign result = res_reg;
endmodule