module flat_to_array_224
	#(parameter NUMBER_SIZE = 16,
	  parameter MATRIX_SIZE = 28)
	(
	input [MATRIX_SIZE/2 * NUMBER_SIZE - 1:0] value,
	output [NUMBER_SIZE:0] matrix [MATRIX_SIZE/2 - 1:0]
	);	
	
	genvar i;
	generate
	begin
		for (i = 0; i <	MATRIX_SIZE/2; i = i + 1)
			begin : flatter
				assign matrix[i] = value[(i + 1) * NUMBER_SIZE - 1 : i * NUMBER_SIZE];
			end
	end
	endgenerate
endmodule