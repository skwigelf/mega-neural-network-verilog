module kernel_row
	#(parameter MATRIX_SIZE = 5,
	parameter NUMBER_SIZE = 15)
	(
		input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0],   
		input wire [4:0] row_index,
		output wire [NUMBER_SIZE:0] row [MATRIX_SIZE - 1:0]
	);		 
	
	genvar i;
	generate 
		begin
		for (i = 0; i < MATRIX_SIZE; i = i + 1)
			begin : kernelRowExtractor
				assign row[i] = matrix[row_index][i];
			end
		end
	endgenerate
	
endmodule