module multiplier_row_only 
	#(parameter MATRIX_SIZE = 5, parameter NUMBER_SIZE = 15)(
	input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0],
    input wire [NUMBER_SIZE:0] kernel [MATRIX_SIZE - 1:0],
    output wire [NUMBER_SIZE:0] conv [MATRIX_SIZE - 1:0]);

	genvar col;
	generate
		begin
			for (col=0; col < MATRIX_SIZE; col=col+1)
				begin: mplyGen1_1
					fmpy_p fmpy_p(.D1(matrix[col]),.D2(kernel[col]),.DW(conv [col]));
				end

		end	
	endgenerate	  	
endmodule