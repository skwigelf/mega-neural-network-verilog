module adder_rows_2 
	#(parameter MATRIX_SIZE = 2, parameter NUMBER_SIZE = 15)(
	input wire [NUMBER_SIZE:0] vector [MATRIX_SIZE - 1:0],
    output wire [NUMBER_SIZE:0] res);
    
	wire [NUMBER_SIZE:0] buff[MATRIX_SIZE - 1:0]; 	
	
	wire sub = 0;
	
	assign buff[0] = vector[0];	
	genvar i;
	generate
		begin
			for (i=1; i < MATRIX_SIZE; i=i+1)
				begin: mplyGen2	
					fadd0p fadd0p(.D0(buff[i - 1]),.D1(vector[i]),.SUB(sub),.DW(buff[i])); 
				end
		end	
		
	endgenerate	 
	assign res = buff[MATRIX_SIZE - 1];
endmodule