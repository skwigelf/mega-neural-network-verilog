module maxpool 
#(
	parameter MATRIX_SIZE = 24,	
	parameter RESULT_SIZE = 6, 
	parameter KERNEL_SIZE = 4,
	parameter STEP = 4,
	parameter NUMBER_SIZE = 15
)(
	input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0], 
	input wire clk,
	input wire start,
	output wire [NUMBER_SIZE:0] result [RESULT_SIZE - 1:0][RESULT_SIZE - 1:0],
	output wire ready
	);
		 	
	reg finished = 0;
	reg [5:0] col = 0;
	reg [5:0] row = 0;	  
	
	reg [NUMBER_SIZE:0] result_matrix_element;
	reg [NUMBER_SIZE:0] extracted_submatrix [KERNEL_SIZE - 1:0][KERNEL_SIZE - 1:0];  
	reg [NUMBER_SIZE:0] result_matrix [RESULT_SIZE - 1:0][RESULT_SIZE - 1:0];
	
	always@(posedge clk)	  
	begin	
		if (~start)
			begin
				col = 0;
				row = 0;
				finished = 0;
			end
		else if (~finished)
		   begin	 
			result_matrix[row][col] = result_matrix_element;	 
			if (row > RESULT_SIZE)
				begin
					finished = 1;
				end	 
			else
				begin
					if (col < RESULT_SIZE)
						col = col + 1;		  
					else 
						begin
						col = 0;
						row = row + 1;
					end			
			end
			
		end	
		
	end		
					
	submatrix_2 submatrix_2(.matrix(matrix),.column_index(col*STEP),.row_index(row * STEP),.conv(extracted_submatrix));	
	maxpool_operation maxpool_operation(.matrix(extracted_submatrix), .max_value(result_matrix_element));
	
	assign result = result_matrix;
	assign ready = finished;
	//assign result[2][1] = col;
	//assign result[2][2] = row;
endmodule