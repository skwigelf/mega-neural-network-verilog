module argmax #(parameter CLASSES = 10, parameter NUMBER_SIZE = 16)(
	input [NUMBER_SIZE - 1:0] values [CLASSES - 1:0],
	output reg [3:0] result
);

	wire [NUMBER_SIZE -1:0] max_values_1 [(CLASSES - 2) / 2 - 1:0];
	wire [NUMBER_SIZE -1 :0] max_values_2 [(CLASSES - 2) / 4 - 1:0];
	wire [NUMBER_SIZE - 1:0] max_values_3;
	wire [NUMBER_SIZE - 1:0] max_values_4;
	wire [NUMBER_SIZE - 1:0] max_values_5;

	
	genvar i;
	generate
		begin	
			for (i=0; i < CLASSES - 2 - 1; i=i+2)
				begin : rowPointer
					fp_comparator fp_comparator(.first_val(values[i]), .sec_val(values[i + 1]), .result(max_values_1[i/2]));
				end 
		end
	endgenerate
	
	generate
		begin	
			for (i=0; i < (CLASSES - 2) / 2 - 1; i=i+2)
				begin : rowPointer2
					fp_comparator fp_comparator2(.first_val(max_values_1[i]), .sec_val(max_values_1[i + 1]), .result(max_values_2[i/2]));
				end 
		end
	endgenerate
	
	fp_comparator fp_comparator3(.first_val(max_values_2[0]), .sec_val(max_values_2[1]), .result(max_values_3));
	fp_comparator fp_comparator4(.first_val(max_values_3), .sec_val(values[8]), .result(max_values_4));
	fp_comparator fp_comparator5(.first_val(max_values_4), .sec_val(values[9]), .result(max_values_5));
	
	always@(*)
	begin
		if (values[0] == max_values_5)
			 result = 0;
		else if (values[1] == max_values_5)
			 result = 1;
		else if (values[2] == max_values_5)
			 result = 2;
		else if (values[3] == max_values_5)
			 result = 3;
		else if (values[4] == max_values_5)
			 result = 4;
		else if (values[5] == max_values_5)
			 result = 5;
		else if(values[6] == max_values_5)
			 result = 6;
		else if (values[7] == max_values_5)
			 result = 7;
		else if (values[8] == max_values_5)
			 result = 8;
		else if (values[9] == max_values_5)
			 result = 9;
	end
	
endmodule