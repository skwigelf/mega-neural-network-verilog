module	   fmpy_p  
#(
  parameter m_wd = 10, 	
  parameter e_wd = 5	
)	  
(
	input  [m_wd + e_wd:0] D1, D2,	
	output [m_wd + e_wd:0] DW
);	  


wire            DOs;
wire [e_wd-1:0] DOe;
wire [m_wd-1:0] DOm;

wire            D1s,D2s;
wire [e_wd-1:0] D1e,D2e;
wire [m_wd-1:0] D1m,D2m;

wire              Zm,Vm;
wire              OVF,D1e_eq_00h,D1e_eq_FFh,D2e_eq_00h,D2e_eq_FFh,
                  D1m_eq_0,D2m_eq_0;
wire       [1:0]  SEL_mnx;
wire  [e_wd-1:0]  ep1;
wire    [e_wd:0]  exp,expd;
wire [2*m_wd+1:0] mp;
reg     [m_wd:0]  mnx;
wire    [m_wd:0]  mpi;	
wire    [m_wd:0]  A,B;

assign	          DW = {DOs,DOe,DOm};
assign	          {D1s,D1e,D1m} = D1;
assign	          {D2s,D2e,D2m} = D2;

assign            D1m_eq_0=D1m==0;
assign            D2m_eq_0=D2m==0;

assign            D1e_eq_00h=D1e==0;
assign            D1e_eq_FFh=D1e=={e_wd{1'b1}};
assign            D2e_eq_00h=D2e==0;
assign            D2e_eq_FFh=D2e=={e_wd{1'b1}};

wire              QNaN,NaN1,NaN2,Io1,Io2,Io,Zo1,Zo2;

assign            Zo1   =D1e_eq_00h;
assign            Zo2   =D2e_eq_00h;

assign            Zo    =Zo1 |Zo2 ;

assign            Io1   =D1e_eq_FFh & D1m_eq_0;
assign            Io2   =D2e_eq_FFh & D2m_eq_0;
assign            Io    =Io1|Io2;

assign            NaN1  =D1e_eq_FFh &(~D1m_eq_0);
assign            NaN2  =D2e_eq_FFh &(~D2m_eq_0);

assign            QNaN  =(NaN1|NaN2) | (Zo1 & Io2) | (Zo2 & Io1);


//Mul operation execution
assign	          A={1'b1,D1m[m_wd-1:0]};
assign	          B={1'b1,D2m[m_wd-1:0]};

assign			  mp=A*B;

assign		   	   mpi = mp[2*m_wd+1:m_wd+1] + 1'b1;
assign			   OVF = &mp[2*m_wd:m_wd-1];
assign 	SEL_mnx[1] = mp[2*m_wd+1];
assign 	SEL_mnx[0] = mp[2*m_wd+1]? mp[m_wd] && (mp[m_wd+1] || (|mp[m_wd-1:0])): (mp[m_wd-1] && mp[m_wd]);
always @(SEL_mnx or mp or mpi)
case (SEL_mnx) // synopsys infer_mux
2'b00: mnx <=  {mp[2*m_wd:m_wd+1],(mp[m_wd] || (mp[m_wd-1] && (|mp[m_wd-2:0])))};
2'b01: mnx <=  {mpi[m_wd-1:0],1'b0};
2'b10: mnx <=   mp[2*m_wd+1:m_wd+1];
2'b11: mnx <=   mpi;
endcase
 
assign             expd = D1e + D2e + 1'b1;
assign             exp  = expd + 1'b1;		
assign             ep1  = (OVF || mp[2*m_wd+1])? {exp[e_wd],exp[e_wd-2:0]}: {expd[e_wd],expd[e_wd-2:0]};

assign             Zm = !QNaN && (Zo || !expd[e_wd] && (!expd[e_wd-1] || !(|expd[e_wd-2:0]) && !(OVF || mp[2*m_wd+1])));
assign             Vm = QNaN || Io || exp[e_wd] && (exp[e_wd-1] || (&exp[e_wd-2:0]) && (OVF || mp[2*m_wd+1]));

assign             DOs = !QNaN && (D1s^^D2s) && !Zm;
assign             DOm=(~(Zm|Vm))? mnx[m_wd-1:0]:
					    (QNaN)? {m_wd{1'b1}}:  0;
assign             DOe= Vm? {e_wd{1'b1}}:
                        Zm? 0:
                        ep1;	

endmodule