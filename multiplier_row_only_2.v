module multiplier_row_only_2 
	#(parameter MATRIX_SIZE = 2, parameter NUMBER_SIZE = 15)(
	input wire [NUMBER_SIZE:0] vector [MATRIX_SIZE - 1:0],
    input wire [NUMBER_SIZE:0] weights [MATRIX_SIZE - 1:0],
    output wire [NUMBER_SIZE:0] res [MATRIX_SIZE - 1:0]);

	genvar col;
	generate
		begin
			for (col=0; col < MATRIX_SIZE; col=col+1)
				begin: mplyGen1_1
					fmpy_p fmpy_p(.D1(vector[col]),.D2(weights[col]),.DW(res [col]));
				end

		end	
	endgenerate	  	
endmodule