module maxpool_operation #(parameter NUMBER_SIZE = 15, parameter SIZE = 4) (
	input wire [NUMBER_SIZE:0] matrix [SIZE - 1:0][SIZE - 1:0],		  
    output wire [NUMBER_SIZE:0] max_value
	);						  
	
	genvar i;
	genvar j;
	
	wire [NUMBER_SIZE:0] max_values_1 [SIZE * SIZE / 2 - 1:0];
	wire [NUMBER_SIZE:0] max_values_2 [SIZE * SIZE / 4 - 1:0];
	wire [NUMBER_SIZE:0] max_values_3 [SIZE * SIZE / 8 - 1:0];

	
	generate
		begin	
			for (i=0; i < SIZE; i=i+1)
				begin : rowPointer
					for (j=0; j < SIZE - 1; j=j+2)
						begin : colPointer 
							fp_comparator fp_comparator(.first_val(matrix[i][j]), .sec_val(matrix[i][j + 1]), .result(max_values_1[i * SIZE / 2 + j/2]));
						end
				end 
		end
	endgenerate
	
	generate
		begin
			for (i=0; i < 2 * SIZE - 1; i=i+2)
				begin : pointer1
					fp_comparator fp_comparator(.first_val(max_values_1[i]), .sec_val(max_values_1[i + 1]), .result(max_values_2[i / 2]));
				end
		end
	endgenerate
	
	generate
		begin
			for (i=0; i < SIZE - 1; i=i+2)
				begin : pointer2
					fp_comparator fp_comparator(.first_val(max_values_2[i]), .sec_val(max_values_2[i + 1]), .result(max_values_3[i/2]));
				end
		end
	endgenerate
	
	
	fp_comparator fp_comparator3(.first_val(max_values_3[0]), .sec_val(max_values_3[0 + 1]), .result(max_value));


	
endmodule