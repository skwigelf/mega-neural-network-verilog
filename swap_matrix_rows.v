module swap_matrix_rows 
  #(parameter KERNEL_SIZE = 5,	 
	parameter MATRIX_SIZE = 28,	
	parameter CONV_SIZE = 24,
	parameter NUMBER_SIZE = 15)
	(
    input [NUMBER_SIZE:0] matrix [KERNEL_SIZE-1:0][MATRIX_SIZE-1:0],
    input [NUMBER_SIZE:0] value [MATRIX_SIZE-1:0],
    output [NUMBER_SIZE:0] result [KERNEL_SIZE-1:0][MATRIX_SIZE-1:0]
);
    genvar i;
    generate 
        for (i = 0; i < KERNEL_SIZE - 1; i = i + 1) begin : swapper
            assign result[i] = matrix[i + 1];
        end
    endgenerate    
    assign result[KERNEL_SIZE - 1] = value;

endmodule