module convolution_mega
 #(parameter KERNEL_SIZE = 5,	 
	parameter MATRIX_SIZE = 28,	
	parameter CONV_SIZE = 24,
	parameter MAX_POOL_SIZE = 6,
	parameter STEP = 4,
	parameter NUMBER_SIZE = 16)
	(
    input wire [NUMBER_SIZE- 1:0] kernel [KERNEL_SIZE - 1:0][KERNEL_SIZE - 1:0],
    input wire convolution_start,
    input wire clk,
    output wire [NUMBER_SIZE- 1:0] conv [MAX_POOL_SIZE-1:0][MAX_POOL_SIZE-1:0],
    output wire ready
);

	reg [NUMBER_SIZE- 1:0] result_matrix [MAX_POOL_SIZE-1:0][MAX_POOL_SIZE-1:0];

	//In current design we load matrix 28 * 5. When pointer equal 5, place 0:3 array element to 1:4 and load new 0 value
	reg [NUMBER_SIZE- 1:0] temp_matrix [KERNEL_SIZE-1:0][MATRIX_SIZE-1:0];

	wire [NUMBER_SIZE- 1:0] temp_matrix_wire [KERNEL_SIZE-1:0][MATRIX_SIZE-1:0];
	//bit array loaded from ROM
    wire [MATRIX_SIZE/2 * (NUMBER_SIZE) - 1:0] matrix_buffer_flat ;
	//one row with half of columns from flat buffer
	wire [NUMBER_SIZE- 1:0] matrix_buffer [MATRIX_SIZE/2 - 1:0];

	wire [NUMBER_SIZE- 1:0] matrix_buffer_full_row [MATRIX_SIZE - 1:0];
	//Указатели используемые для инициализации первых строк матрицы (количество начальных строк = KERNEL_SIZE)
	//init_col_counter принимает значения от 0 до 2. Режим 0
	//clk_waiting_count используется для прогона "холостых" тактов, ожидая загрузки строки из памяти из памяти
	reg [1:0] clk_waiting_count = 0;
	reg init_col_counter = 0;
	reg [3:0] init_row_counter = 0;

	//this flag becomes 1 when memory inited
    reg memory_inited = 0; 

	//this flag is needed to load row
	reg row_loaded = 0;
	reg row_loading = 1;
	reg row_loading_waiting = 1;
	reg row_swapped = 0;
	
	//this flag becomes 1 when convolution finished calculating
	reg finished = 0;

	//Global row and col pointer. May be from 0 to MATRUX_SIZE(28)
	reg [5:0] col = 0;
	reg [5:0] row = 0;
	
	reg [3:0] row_offset = 0;
	
	reg [NUMBER_SIZE- 1:0] conv_result_matrix [STEP-1:0][CONV_SIZE-1:0];	
	reg [NUMBER_SIZE- 1:0] result_matrix_element = 0;
	
	wire [NUMBER_SIZE- 1:0] row_multiplier_result_wire [KERNEL_SIZE - 1:0];
	reg [NUMBER_SIZE- 1:0] row_multiplier_result [KERNEL_SIZE - 1:0];
	wire [NUMBER_SIZE- 1:0] extracted_submatrix [KERNEL_SIZE - 1:0]; 
	wire [NUMBER_SIZE- 1:0] extracted_kernel_row [KERNEL_SIZE - 1:0];    
	
	reg [5:0] memory_image_pointer = 0; 

	wire [NUMBER_SIZE- 1:0] extracted_submatrix_max_pool_op [STEP - 1:0] [STEP - 1:0];

	wire [NUMBER_SIZE- 1:0] result_max_pool_op;

	reg [2:0] max_pool_row_pointer = 0;
	reg [2:0] max_pool_col_pointer = 0;

	reg max_pool_counting = 0;
	reg [3:0] max_pool_counting_pointer = 0;
	reg [3:0] max_pool_current_row = 0;
	
	reg start_assign_memory = 0;

    reg [NUMBER_SIZE- 1:0] adder_row [KERNEL_SIZE - 1:0];
	
	always@(posedge clk)	  
	begin	
		//сигнал старта(сброса). Выставляет счетчики в 0
		if (!convolution_start)
			begin
				col = 0;
				row = 0;
				finished = 0;
			end
		// Загрзука первых строк изображения. Количество строк равно размеру ядра свертки, для того чтобы можно было пройти ядром свертки вдоль матрицы
		else if (!memory_inited)
            begin
        // clk_waiting_count используется для прогона холостых тактов для того чтобы дождаться пока строчка загрузится из памяти 
				if (clk_waiting_count < 2)
					clk_waiting_count = clk_waiting_count + 1;
				else if (clk_waiting_count == 2)
					begin
						clk_waiting_count = 0;
						if (init_col_counter == 1)
							begin
								temp_matrix[init_row_counter][MATRIX_SIZE/2 - 1:0] = matrix_buffer;
								init_row_counter = init_row_counter + 1;
							end
						else
							begin
								temp_matrix[init_row_counter][MATRIX_SIZE - 1: MATRIX_SIZE/2] = matrix_buffer;
							end
						init_col_counter = !init_col_counter;
						memory_image_pointer = memory_image_pointer + 1;
					end
				else
					begin
						memory_inited = 1;
					end
				
            end 
        
        else if (~finished)
		    begin
				if (!row_loading)
					begin

						matrix_buffer_full_row[MATRIX_SIZE/2 - 1:0] = matrix_buffer;
						memory_image_pointer = memory_image_pointer + 1;
						row_loading = 1;
						row_loading_waiting = 0;	
						row_loaded = 1;
					end	
				else if (!row_loading_waiting)
					begin
						row_loading_waiting = 1;
					end
				else if (row_loaded)
					begin
							// caienuaaai i?aao? ?anou no?iee
						matrix_buffer_full_row[MATRIX_SIZE - 1: MATRIX_SIZE/2] = matrix_buffer;
						row_loaded = 0;
						row_swapped = 1;   
						row_loading_waiting = 0;  
						memory_image_pointer = memory_image_pointer + 1;
					end
				// oaaeyai aa?oi?? no?i?eo, aiaaaeai ie?i??
				else if (row_swapped) 
					begin
						temp_matrix = temp_matrix_wire;
						row_swapped = 0;
					end
				
				// eiaaa ain?eoaeinu 4 no?iee, i?ioiaei ii iao?eoa 4*24 oaaeeoae 4*4
				else if (max_pool_counting)
					begin
						if (max_pool_counting_pointer == MAX_POOL_SIZE)
							begin
								max_pool_counting_pointer = 0;
								max_pool_counting = 0;
								max_pool_current_row = max_pool_current_row + 1;
							end
						else
							begin
								result_matrix[max_pool_current_row][max_pool_counting_pointer] = result_max_pool_op;
								max_pool_counting_pointer = max_pool_counting_pointer + 1;
							end
					end
				else
					begin
						if (row_offset < KERNEL_SIZE)
							begin
								if (row_offset > 0)
									row_multiplier_result[row_offset - 1] = result_matrix_element;
								else 
									conv_result_matrix[max_pool_row_pointer][max_pool_col_pointer] = result_matrix_element;
								
								adder_row = row_multiplier_result_wire;
								row_offset = row_offset + 1;
							end
						else
							begin
								row_multiplier_result[KERNEL_SIZE - 1] = result_matrix_element;
								adder_row = row_multiplier_result;
								row_offset = 0;
								
								
								if (row == CONV_SIZE)
									begin
										finished = 1;
										memory_image_pointer = 0;
										init_col_counter = 0;
										init_row_counter = 0;
									end	 
								else
									begin
										if (col == CONV_SIZE - 1)
											begin
												row_loading = 0;
											end
										if (col < CONV_SIZE)
											begin
												max_pool_col_pointer = max_pool_col_pointer + 1;
												col = col + 1;
											end
													
										else 
											begin
												col = 0;
												max_pool_col_pointer = 0;
												row = row + 1;
												if (max_pool_row_pointer == STEP - 1)
													begin
														max_pool_row_pointer = 0;
														max_pool_counting = 1;
													end
													
												else
													max_pool_row_pointer = max_pool_row_pointer + 1;
											end			
									end
							end
					end
			end	
	end	
	
	rom_image rom_image(.address(memory_image_pointer), .clock(clk), .q(matrix_buffer_flat));
	flat_to_array_224 flat_to_array_224(.value(matrix_buffer_flat), .matrix(matrix_buffer));
    
	swap_matrix_rows swap_matrix_rows(.matrix(temp_matrix), .value(matrix_buffer_full_row), .result(temp_matrix_wire));
	
	submatrix_row submatrix_row(.matrix(temp_matrix), .column_index(col), .row_index(row_offset), .conv(extracted_submatrix));
	kernel_row kernel_row(.matrix(kernel),.row_index(row_offset), .row(extracted_kernel_row));
	multiplier_row_only multiplier_row_only(.matrix(extracted_submatrix), .kernel(extracted_kernel_row), .conv(row_multiplier_result_wire));
	adder_rows adder_rows(.matrix(adder_row), .conv(result_matrix_element));

	submatrix_2_2 submatrix_2_2(.matrix(conv_result_matrix), .column_index(max_pool_counting_pointer * STEP), .conv(extracted_submatrix_max_pool_op));	
	maxpool_operation maxpool_operation(.matrix(extracted_submatrix_max_pool_op), .max_value(result_max_pool_op));
		
	assign conv = result_matrix;
	assign ready = finished;
endmodule
