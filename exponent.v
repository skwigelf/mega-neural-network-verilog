module exponent 
	#(parameter NUMBER_SIZE = 15)(
	input [NUMBER_SIZE:0] value,
	input clk,
	output [NUMBER_SIZE:0] result
	);	
	
	reg [NUMBER_SIZE:0] first_mply_val;		 
	reg [NUMBER_SIZE:0] second_mply_val;   	
	
	reg [NUMBER_SIZE:0] quad_result; 
	reg [NUMBER_SIZE:0] add_result;			 
	
	reg [NUMBER_SIZE:0] result_accumulator = 16'b0000000000000000;	 
	reg [NUMBER_SIZE:0] result_accumulator_wire;	 
	
	wire [NUMBER_SIZE:0] addone_value;
	wire [NUMBER_SIZE:0] result_temp;	   		
	
	reg [NUMBER_SIZE:0] add_first = 16'b0011110000000000;		
	
	wire sub = 0;  
	reg [3:0] counter = 0;	 
	
	always@(posedge clk)
		begin
				if (counter == 0)	
					begin
						counter = counter + 1;
						first_mply_val = value;	 
						second_mply_val = value; 
					end
				else if (counter == 1)
					begin 	
						add_result = addone_value;
						quad_result = result_temp;
						first_mply_val = result_temp;  
						second_mply_val = 16'b0011100000000000;
						counter = counter + 1;
					end	  
				else if (counter == 2)
					begin 	
						result_accumulator = result_accumulator_wire;
						add_result = result_temp;
						first_mply_val = quad_result;  
						second_mply_val = value;
						counter = counter + 1;
					end	 
				else if (counter == 3)
					begin 	
						result_accumulator = result_accumulator_wire;
						first_mply_val = result_temp;  
						second_mply_val = 16'b0011000101010101;
						counter = counter + 1;
					end	   
				else if (counter == 4)
					begin 	
						add_result = result_temp;				
						counter = counter + 1;
					end	
				
		end
		
	fmpy_p fmpy_p(.D1(first_mply_val),.D2(second_mply_val),.DW(result_temp));	  
	fadd0p fadd0p_2(.D0(add_result),.D1(result_accumulator),.SUB(sub),.DW(result_accumulator_wire)); 
	
	fadd0p fadd0p(.D0(value),.D1(add_first),.SUB(sub),.DW(addone_value));
	
	assign result = result_accumulator_wire;
	
endmodule