module output_segments(
	input wire [7:0] data,
	input wire clock, 
	output wire [6:0] segment [2:0]
);

	reg [3:0] counter [2:0];
	always@(posedge clock)
		begin
			counter[2] = data % 10;
			counter[1] = (data/10)%10;
			counter[0] = ((data/10)/10)%10;
		end
	seven_segment seven_segment0(.data(counter[0]), .segment(segment[2]));
	seven_segment seven_segment1(.data(counter[1]), .segment(segment[1]));
	seven_segment seven_segment2(.data(counter[2]), .segment(segment[0]));

endmodule