module main 
	#(parameter FEATURE_MAPS = 5;
	parameter KERNEL_SIZE = 5,	 
	parameter MATRIX_SIZE = 28,	
	parameter MAXPOOL_SIZE = 6,
	parameter CLASSES = 10,
	parameter NUMBER_SIZE = 15)
	(
		input clk,
		output wire [6:0] out_num
	);	
	wire [3:0] argmax_result;
	
	//kernel - матрица с ядром свертки, kernel_buffer - строчка ядра свертки, 
	//kernel_buffer_flat - строчка в виде строки битов (полученная из памяти)
	//kernel_row_pointer - указатель на строчки в памяти
	reg [NUMBER_SIZE:0] kernel [KERNEL_SIZE - 1:0][KERNEL_SIZE - 1:0];
	reg [KERNEL_SIZE * (NUMBER_SIZE + 1) - 1:0] kernel_buffer_flat ;
	reg [NUMBER_SIZE:0] kernel_buffer [KERNEL_SIZE - 1:0];

	reg [3:0] kernel_row_pointer = 0;

	//Счетчик для карт признако
	reg [4:0] feature_map_counter = 0;

	//сигнал сброса для свертки. Выставляется в 1 во время работы полносвязного слоя и загрузки ядра из памяти
	reg convolution_start = 0;

	//Результат слоя свертки. Слой свертки включает в себя слой пулинга
	wire [NUMBER_SIZE:0] current_max_pool [MAXPOOL_SIZE - 1:0] [MAXPOOL_SIZE - 1:0];

	//Сигнал о том что вычисления свертки завершены. Если 1 то упр сигнал полносвяз слоя dense_layer_start = 0
	wire convolution_ready;

	//Сигнал старта вычислений полносвязного слоя
	reg dense_layer_start = 0;

	//Сигнал о том что вычисления полносвяз завершены. Если 1 
	wire dense_layer_finished;

	//Результат вычисления полносвяз слоя для одной карты признаков 
	wire [NUMBER_SIZE:0] dense_result_one_map [CLASSES - 1:0];

	//Результат вычисления полносвяз слоя для всех карт признаков
	reg [NUMBER_SIZE:0] dense_result_all_maps [FEATURE_MAPS - 1:0][CLASSES - 1:0];
	
	//Результат вычислений полносвязного слоя
	wire [NUMBER_SIZE:0] dense_result [CLASSES - 1:0];

	reg kernel_loaded = 0;
	
	always @(posedge clk)
	begin
		//Здесь инициализируется ядро свертки для текущей карты
		if (!kernel_loaded)
			begin
				//Когда указатель доходит до последней строчки выставляются упр сигналы о том что ядро загружено и можно начинатьс свертку
				if (kernel_row_pointer == KERNEL_SIZE)
					begin
						kernel_loaded = 1;
						convolution_start = 1;
					end
				else
					//Текущая строка загруженная из памяти записывается в итоговое ядро свертки к
					begin
						kernel[kernel_row_pointer] = kernel_buffer;
						kernel_row_pointer = kernel_row_pointer + 1;
					end
			end
		else
			begin
			//Когда свертка закончилась, выставляем сигнал conv_start в 0 чтобы свертка закнчила считаться и ее внутренние указатели обнулились
				if (convolution_ready)
					begin
						convolution_start = 0;
						dense_layer_start=1;
					end
				//Когда вычисление полн слоя завершено выставляется dense_layer_start чтобы слой закончил считаться и его внутренние указатели обнулились
				//convolution_start выставляем в 1 чтоб звапустить свертку
				//инкрементируем значение счетчика карт признаков
				//записываем результат в dense_result_all_maps
				else if (dense_layer_finished)
					begin
						dense_result_all_maps[feature_map_counter] = dense_result_one_map;
						feature_map_counter = feature_map_counter + 1;
						dense_layer_start = 0;
						convolution_start = 1;
					end
				
			end	 
	end

		ROM_KERNEL ROM_KERNEL(.address(kernel_row_pointer), .clock(clk), .q(kernel_buffer_flat));
		flat_to_array_80 flat_to_array_80(.value(kernel_buffer_flat), .matrix(kernel_buffer));
	
		convolution_mega convolution_mega(.kernel(kernel), .convolution_start(convolution_start), .clk(clk), .conv(current_max_pool), .ready(convolution_ready));
		dense_layer dense_layer(.matrix(current_max_pool), .clk(clk), .start(dense_layer_start), .finished(dense_layer_finished), .result(dense_result_one_map));

		sum_neuron_results sum_neuron_results(.matrix(dense_result_all_maps), .clk(clk), .result(dense_result));
		
		argmax argmax(.values(dense_result), .result(argmax_result));
		seven_segment seven_segment(.data(argmax_result), .segment(out_num));
endmodule