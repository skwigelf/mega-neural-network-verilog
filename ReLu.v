module ReLu #(parameter NUMBER_SIZE = 15)(
	input [NUMBER_SIZE:0] value,
	output reg [NUMBER_SIZE:0] result
	);					
	always@(*)
		begin
		if (value[0] == 1'b1)
			 result = 16'b0000000000000000;//32'b000000000000000000000000000000;
		else 
		 	 result = value;	   
	end
endmodule