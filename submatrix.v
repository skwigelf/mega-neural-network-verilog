module submatrix 
	#(parameter MATRIX_SIZE = 27,
	parameter RESULT_SIZE = 5,
	parameter NUMBER_SIZE = 15)
	(
		input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE:0][MATRIX_SIZE:0],
		input wire [4:0] column_index,	   
		input wire [4:0] row_index,
		output wire [NUMBER_SIZE:0] conv [RESULT_SIZE - 1:0][RESULT_SIZE - 1:0]
	);		 
	
	genvar i;
	genvar j;
	generate 
		begin
		for (i = 0; i < RESULT_SIZE; i = i + 1)
			begin : rowExtractor
				for (j = 0; j < RESULT_SIZE; j++) 
					begin : colExtractor
						assign conv[i][j] = matrix[row_index + i][column_index + j];
					end
			end
		end
	endgenerate
	
endmodule