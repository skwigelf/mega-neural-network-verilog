module seven_segment(
	input wire [3:0] data, 
	output reg [6:0] segment
);
	
	always@(*)
	begin
		case (data)
			4'b0000: segment = 7'b1000000; // "0"  
			 4'b0001: segment = 7'b1111001; // "1" 
			 4'b0010: segment = 7'b0100100; // "2" 
			 4'b0011: segment = 7'b0110000; // "3" 
			 4'b0100: segment = 7'b0011001; // "4" 
			 4'b0101: segment = 7'b0010010; // "5" 
			 4'b0110: segment = 7'b0000010; // "6" 
			 4'b0111: segment = 7'b1111000; // "7" 
			 4'b1000: segment = 7'b0000000; // "8"  
			 4'b1001: segment = 7'b0010000; // "9" 
			 default: segment = 7'b1000000; // "0"
		endcase
	end

endmodule