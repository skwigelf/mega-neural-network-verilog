module	   fmpy  (DW, D1,D2);

output   [31:0]   DW;

input    [31:0]   D1,D2;

wire            DOs;
wire    [7:0]   DOe;
wire   [22:0]   DOm;

wire             D1s,D2s;
wire     [7:0]   D1e,D2e;
wire    [22:0]   D1m,D2m;

wire              Zm,Vm;
wire              OVF,D1e_eq_00h,D1e_eq_FFh,D2e_eq_00h,D2e_eq_FFh,
                  D1m_eq_0,D2m_eq_0;
wire      [1:0]	  SEL_mnx;
wire      [7:0]   ep1;
wire      [8:0]   exp,expd;
wire     [47:0]   mp;
reg      [23:0]   mnx;
wire     [23:0]   mpi;	
wire      [23:0]  A,B;

assign	          DW = {DOs,DOe,DOm};
assign	          {D1s,D1e,D1m} = D1;
assign	          {D2s,D2e,D2m} = D2;

assign            D1m_eq_0=D1m==23'h000000;
assign            D2m_eq_0=D2m==23'h000000;

assign            D1e_eq_00h=D1e==8'h00;
assign            D1e_eq_FFh=D1e==8'hff;
assign            D2e_eq_00h=D2e==8'h00;
assign            D2e_eq_FFh=D2e==8'hff;

wire              QNaN,NaN1,NaN2,Io1,Io2,Io,Zo1,Zo2;

assign            Zo1   =D1e_eq_00h;
assign            Zo2   =D2e_eq_00h;

assign            Zo    =Zo1 |Zo2 ;

assign            Io1   =D1e_eq_FFh & D1m_eq_0;
assign            Io2   =D2e_eq_FFh & D2m_eq_0;
assign            Io    =Io1|Io2;

assign            NaN1  =D1e_eq_FFh &(~D1m_eq_0);
assign            NaN2  =D2e_eq_FFh &(~D2m_eq_0);

assign            QNaN  =(NaN1|NaN2) | (Zo1 & Io2) | (Zo2 & Io1);


//Mul operation execution
assign	          A={1'b1,D1m[22:0]};
assign	          B={1'b1,D2m[22:0]};

assign			  mp=A*B;

assign		   	   mpi = mp[47:24] + 1'b1;
assign			   OVF = &mp[46:22];
assign 	SEL_mnx[1] = mp[47];
assign 	SEL_mnx[0] = mp[47]? mp[23] && (mp[24] || (|mp[22:0])): (mp[22] && mp[23]);
always @(SEL_mnx or mp or mpi)
case (SEL_mnx) // synopsys infer_mux
2'b00: mnx <=  {mp[46:24],(mp[23] || (mp[22] && (|mp[21:0])))};
2'b01: mnx <=  {mpi[22:0],1'b0};
2'b10: mnx <=   mp[47:24];
2'b11: mnx <=   mpi;
endcase
 
assign             expd = D1e + D2e + 1'b1;
assign             exp  = expd + 1'b1;		
assign             ep1  = (OVF || mp[47])? {exp[8],exp[6:0]}: {expd[8],expd[6:0]};

assign             Zm = !QNaN && (Zo || !expd[8] && (!expd[7] || !(|expd[6:0]) && !(OVF || mp[47])));
assign             Vm = QNaN || Io || exp[8] && (exp[7] || (&exp[6:0]) && (OVF || mp[47]));

assign             DOs = !QNaN && (D1s^^D2s) && !Zm;
assign             DOm=(~(Zm|Vm))? mnx[22:0]:
					    (QNaN)? 23'h7FFFFF:  23'h0;
assign             DOe= Vm? 8'hFF:
                        Zm? 8'h00:
                        ep1;	

endmodule
