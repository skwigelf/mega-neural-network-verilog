module sum_neuron_results
#(parameter NUMBER_SIZE = 15,
	parameter CLASSES = 9,
  parameter FEATURE_MAPS = 5)
(
	input [NUMBER_SIZE:0] matrix [FEATURE_MAPS - 1:0][CLASSES:0],
	input clk,
	output [NUMBER_SIZE:0] result [CLASSES:0]
);	

	reg finished = 0;

	reg [3:0] map_pointer = 0;
	reg [3:0] class_pointer = 0;
	
	reg [NUMBER_SIZE:0] accumulator = 0;

	reg sub = 0;
	wire [NUMBER_SIZE:0] sum_result; 

	reg [NUMBER_SIZE:0] second_number;

	reg [NUMBER_SIZE:0] result_reg [CLASSES:0];	 
	
	reg init_first_result = 1;

	always@(posedge clk)
		begin	
			if (finished)
				begin
					
				end
			else if (init_first_result)
				begin
					init_first_result = 0;		 
					second_number = matrix[0][0];
				end
			else 
				begin
					
					if (map_pointer < FEATURE_MAPS - 1)
						begin
							accumulator = sum_result;	 
							map_pointer = map_pointer + 1;
							second_number = matrix[map_pointer][class_pointer];
						end		  
					else if (class_pointer < CLASSES)
						begin	
							map_pointer = 0;
							result_reg[class_pointer] = sum_result;
							class_pointer = class_pointer + 1;	  
							second_number = matrix[map_pointer][class_pointer];
							accumulator = 0;
						end
					else
						begin	   
							result_reg[class_pointer] = sum_result;
							finished = 1;
						end			
				end
		end

	fadd0p fadd0p(.D0(accumulator), .D1(second_number), .SUB(sub), .DW(sum_result));

	assign result = result_reg;
endmodule