module multiplier_row 
	#(parameter MATRIX_SIZE = 5, parameter NUMBER_SIZE = 15)(
	input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0],
    input wire [NUMBER_SIZE:0] kernel [MATRIX_SIZE - 1:0],
    output wire [NUMBER_SIZE:0] conv);
    
    wire [NUMBER_SIZE:0] buff_res[MATRIX_SIZE - 1:0];
	wire [NUMBER_SIZE:0] buff[MATRIX_SIZE - 1:0]; 	
	
	wire sub = 0;
	genvar col;
	genvar row;
	generate
		begin
			for (col=0; col < MATRIX_SIZE; col=col+1)
				begin: mplyGen1_1
					fmpy_p fmpy_p(.D1(matrix[col]),.D2(kernel[col]),.DW(buff_res[col]));
				end

		end	
	endgenerate	  				  
	assign buff[0] = buff_res[0];	
	genvar i;
	generate
		begin
			for (i=1; i < MATRIX_SIZE; i=i+1)
				begin: mplyGen2	
					fadd0p fadd0p(.D0(buff[i - 1]),.D1(buff_res[i]),.SUB(sub),.DW(buff[i])); 
				end
		end	
		
	endgenerate	 
	assign conv = buff[MATRIX_SIZE - 1];
endmodule