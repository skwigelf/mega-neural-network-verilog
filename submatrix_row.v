module submatrix_row 
	#(parameter MATRIX_SIZE = 28,
	parameter RESULT_SIZE = 5,
	parameter NUMBER_SIZE = 15)
	(
		//input wire [NUMBER_SIZE:0] matrix [MATRIX_SIZE:0][MATRIX_SIZE:0],
		input wire [NUMBER_SIZE:0] matrix [RESULT_SIZE - 1:0][MATRIX_SIZE - 1:0],
		input wire [4:0] column_index,	   
		input wire [4:0] row_index,
		output wire [NUMBER_SIZE:0] conv [RESULT_SIZE - 1:0]
	);		 
	
	genvar i;
	generate 
		begin
		for (i = 0; i < RESULT_SIZE; i = i + 1)
			begin : rowExtractor
				assign conv[i] = matrix[row_index][column_index + i];
			end
		end
	endgenerate
	
endmodule