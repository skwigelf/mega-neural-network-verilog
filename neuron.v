module neuron
	#(parameter NUMBER_SIZE = 15,
	  parameter NEURON_NUMBER = 2)
	(input [NUMBER_SIZE:0] matrix [NEURON_NUMBER - 1:0], input [NUMBER_SIZE:0] weights [NEURON_NUMBER - 1:0], output [NUMBER_SIZE:0] res);	
   
	wire sub=0;			
	reg [NUMBER_SIZE:0] buff_res [NEURON_NUMBER:0];
	reg [NUMBER_SIZE:0] buff [NEURON_NUMBER:0]; 	
	genvar i;
	generate
		begin
			for (i=0; i < NEURON_NUMBER; i=i+1)
				begin: mplyGen
					fmpy_p fmpy_p(.D1(weights[i]),.D2(matrix[i]),.DW(buff_res[i]));
				end	
		end	
	endgenerate	
	assign buff[0] = buff_res[0];
	generate
		begin
			for (i=1; i < NEURON_NUMBER; i=i+1)
				begin: mplyGen2
					fadd0p fadd0p(.D0(buff[i - 1]),.D1(buff_res[i]),.SUB(sub),.DW(buff[i])); 
				end	
		end	
	endgenerate	 
	assign res = buff[NEURON_NUMBER - 1];			  
endmodule