module fadd0p 
#(
  parameter m_wd = 10, 	
  parameter e_wd = 5,	
  parameter PW   = 4//$clog2(m_wd + e_wd + 1)
)	  
(
	input  [m_wd + e_wd:0] D0, D1,	
	input  		           SUB,
	output [m_wd + e_wd:0] DW
);	  


wire  [       1:0] RM;
wire  		       Sat;
wire               D0s,D1s,DWs;
wire    [e_wd-1:0] D0e,D1e,DWe;
wire    [m_wd  :0] D0m,D1m,DWm;
wire    [m_wd+2:0] D0mi,D1mi; //signed mantissa + 1 LSB
wire    [m_wd+2:0] D0ms,D1ms,D0ms_,D1ms_; //signed & shifted mantissa + 1 LSB
wire    [m_wd+2:0] SUMm,SUMp; //SUM- and  SUM+	
wire    [m_wd+2:1] SUMp2; 
wire    [m_wd+2:2] SUMmi; 
wire    [e_wd-1:0] D0e_,D1e_;
wire               D0e_eq_0,D1e_eq_0;
wire               D0e_eq_max,D1e_eq_max;
wire               D0m_eq_0,D1m_eq_0;
wire               D0e_lt_D1e,D0m_lt_D1m;
wire               D0e_gt_D1e,D0m_gt_D1m;
wire               D0e_eq_D1e,D0m_eq_D1m;
wire               D0_NaN,D0_inf,D0_Z,D0_dn;
wire               D1_NaN,D1_inf,D1_Z,D1_dn;	 
wire     	       DW_NaN, DW_inf, DW_Z;
wire    [e_wd-1:0] diff_D0e_D1e,diff_D1e_D0e,diff_e;
wire    [  PW-1:0] sh0,sh1,shn,sh_sel,sh_rem;		  
wire		       D0_lt_D1,D0_gt_D1;
wire		       invert_D0,invert_D1;
wire [m_wd+e_wd:0] SUMp_rev;		  
wire        [PW:0] pdn;		  
wire    [e_wd-1:0] DWe_0,DWe_1,DWe_p1,DWe_pdn;	  
wire    [m_wd-1:0] DWm_0,DWm_1,DWm_p1,DWm_pdn,DWm_pdn_;	  
wire			   sel_0,sel_1,sel_p1,sel_pdn; 
wire			   rnd0,rnda1,rnda2,rnds1; 
wire			   rm0,rm1,rm2,rm3; 
wire			   DW_dn,OVF,UF,sat_en,add,opp_sign_sum_eq_0; 
wire			   rem_en, rem_b2, rem_carry1, rem_carry2,sub_carry;//rem_b1, rem_b2, rem_carry0, rem_carry1, rem_carry2,sub_carry;
wire    [m_wd+1:0] rem;	  
wire [m_wd+e_wd:0] rem_msk;	  
wire    [m_wd+2:0] Dm_sel;	  


assign RM = 2'h0;
assign Sat = 1'b0;
assign D0s = D0[m_wd+e_wd] ^^ SUB;
assign D1s = D1[m_wd+e_wd];
assign D0e = D0[m_wd+e_wd-1:m_wd];
assign D1e = D1[m_wd+e_wd-1:m_wd];
assign D0m = {!D0e_eq_0,D0[m_wd-1:0]};
assign D1m = {!D1e_eq_0,D1[m_wd-1:0]};

assign D0e_eq_0 = !(|D0e); 
assign D1e_eq_0 = !(|D1e); 
assign D0e_eq_max = &D0e; 
assign D1e_eq_max = &D1e; 
assign D0m_eq_0 = !(|D0m[m_wd-1:0]); 
assign D1m_eq_0 = !(|D1m[m_wd-1:0]); 


assign D0_NaN = D0e_eq_max && !D0m_eq_0;
assign D1_NaN = D1e_eq_max && !D1m_eq_0;
assign D0_inf = D0e_eq_max &&  D0m_eq_0;
assign D1_inf = D1e_eq_max &&  D1m_eq_0;   
assign D0_Z	  = D0e_eq_0   &&  D0m_eq_0;
assign D1_Z	  = D1e_eq_0   &&  D1m_eq_0;	
assign D0_dn  = D0e_eq_0   && !D0m_eq_0;
assign D1_dn  = D1e_eq_0   && !D1m_eq_0;	

assign rm0 = (RM==2'h0);
assign rm1 = (RM==2'h1);
assign rm2 = (RM==2'h2);
assign rm3 = (RM==2'h3);
// Output
assign DW_NaN = D0_NaN || D1_NaN || D0_inf && D1_inf && (D0s ^^ D1s);
assign DW_inf =(D0_inf || D1_inf) && !DW_NaN;
assign DW_Z   = D0_Z && D1_Z || opp_sign_sum_eq_0;	 
assign opp_sign_sum_eq_0 = D0e_eq_D1e && D0m_eq_D1m && (D0s ^^ D1s) && !DW_NaN;	 

assign D0e_lt_D1e = D0e < D1e;
assign D0e_gt_D1e = D0e > D1e;
assign D0e_eq_D1e = D0e == D1e;
assign D0m_lt_D1m = D0m < D1m;
assign D0m_gt_D1m = D0m > D1m;
assign D0m_eq_D1m = D0m == D1m;
//DW01_cmp6 #(8) UCMPe ( .A(D0e), .B(D1e), .TC(1'b0), .LT(D0e_lt_D1e), .GT(D0e_gt_D1e), .EQ(D0e_eq_D1e), .LE(), .GE(), .NE() );
//DW01_cmp6 #(23) UCMPm ( .A(D0m[23-1:0]), .B(D1m[23-1:0]), .TC(1'b0), .LT(D0m_lt_D1m), .GT(D0m_gt_D1m), .EQ(D0m_eq_D1m), .LE(), .GE(), .NE() );

assign D0e_= {D0e[e_wd-1:1],D0e[0] || D0e_eq_0};	  // D0e=0 and D0e=1 means the same exponent 
assign D1e_= {D1e[e_wd-1:1],D1e[0] || D1e_eq_0};	  // D1e=0 and D1e=1 means the same exponent	   

assign diff_D0e_D1e	= D0e_ - D1e_;
assign diff_D1e_D0e	= D1e_ - D0e_;
//DW01_add #(8) Ud0 ( .A( D0e_), .B(~D1e_), .CI(1'b1), .SUM(diff_D0e_D1e), .CO() );				 
//DW01_add #(8) Ud1 ( .A(~D0e_), .B( D1e_), .CI(1'b1), .SUM(diff_D1e_D0e), .CO() );				 

assign sh0 = diff_D1e_D0e[PW-1:0] | {PW{|diff_D1e_D0e[e_wd-1:PW]}};
assign sh1 = diff_D0e_D1e[PW-1:0] | {PW{|diff_D0e_D1e[e_wd-1:PW]}};

assign D0_lt_D1 = D0e_lt_D1e || D0e_eq_D1e && D0m_lt_D1m;
assign D0_gt_D1 = D0e_gt_D1e || D0e_eq_D1e && D0m_gt_D1m;	  

assign diff_e = D0e_lt_D1e? diff_D1e_D0e: diff_D0e_D1e;

assign invert_D0 = (D0s ^^ D1s);
assign invert_D1 = (D0s ^^ D1s);

assign D0mi = invert_D0? {1'b1,~D0m,1'b1}: {1'b0,D0m,1'b0}; //signed mantissa	+ 1 LSB
assign D1mi = invert_D1? {1'b1,~D1m,1'b1}: {1'b0,D1m,1'b0}; //signed mantissa	+ 1 LSB

assign D0ms_ = (sh0 > m_wd+3)? {(m_wd+3){D0mi[m_wd+2]}}: (D0mi[m_wd+2:0] >> sh0) | ({(m_wd+3){D0mi[m_wd+2]}} << ((m_wd+3) - sh0));
assign D1ms_ = (sh1 > m_wd+3)? {(m_wd+3){D1mi[m_wd+2]}}: (D1mi[m_wd+2:0] >> sh1) | ({(m_wd+3){D1mi[m_wd+2]}} << ((m_wd+3) - sh1));
//DW_rash #(23+3,5) USR0 (.A(D0mi), .DATA_TC(1'b1), .SH(sh0),	.SH_TC(1'b0), .B(D0ms_) ); //signed & shifted mantissa + 1 LSB
//DW_rash #(23+3,5) USR1 (.A(D1mi), .DATA_TC(1'b1), .SH(sh1),	.SH_TC(1'b0), .B(D1ms_) ); //signed & shifted mantissa + 1 LSB		 
assign D0ms = D0_lt_D1? D0ms_: {1'b0,D0m,1'b0};
assign D1ms = D0_gt_D1? D1ms_: {1'b0,D1m,1'b0};	

assign SUMm = D0ms + D1ms;
assign SUMp = D0ms[m_wd+2:0] + D1ms[m_wd+2:0] + 1'b1;
assign SUMp2= D0ms[m_wd+2:1] + D1ms[m_wd+2:1] + 1'b1;
assign SUMmi= SUMm[m_wd+2:2] + 1'b1;
//DW01_add #(23+3) Ua0 ( .A(D0ms), .B(D1ms), .CI(1'b0), .SUM(SUMm), .CO() );	//SUM- 			 
//DW01_add #(23+3) Ua1 ( .A(D0ms[23+2:0]), .B(D1ms[23+2:0]), .CI(1'b1), .SUM(SUMp ), .CO() );	//SUM+			 
//DW01_add #(23+2) Ua2 ( .A(D0ms[23+2:1]), .B(D1ms[23+2:1]), .CI(1'b1), .SUM(SUMp2), .CO() );	//SUM+2			 
//DW01_inc #(23+1) Uai ( .A(SUMm[23+2:2]), .SUM(SUMmi) );  

genvar i;
generate
for (i=2;i<m_wd+2;i=i+1) begin:rev_genblock  
	assign SUMp_rev[i] = SUMp[m_wd-i+1];
  end
endgenerate
assign SUMp_rev[1:0] = 0;
assign SUMp_rev[m_wd+e_wd:m_wd+2] = 0;

//assign SUMp_rev = {7'h0,
//                 SUMp[ 0],SUMp[ 1],SUMp[ 2],SUMp[ 3],SUMp[ 4],SUMp[ 5],SUMp[ 6],SUMp[ 7],SUMp[ 8],SUMp[ 9],
//                 SUMp[10],SUMp[11],SUMp[12],SUMp[13],SUMp[14],SUMp[15],SUMp[16],SUMp[17],SUMp[18],SUMp[19],
//				 SUMp[20],SUMp[21],SUMp[22],2'h0                                                          }; 

assign pdn[PW] = (SUMp_rev==0)? 1'b0: 1'b1;
wire [e_wd+m_wd:0] tmp0[PW:0];
assign tmp0[PW] = SUMp_rev;
genvar j;
generate
for (j=PW-1;j>=0;j=j-1) begin:pdn_genblock 
        assign pdn[j] = ~|tmp0[j+1][2**j-1:0];
	assign tmp0[j] = pdn[j]? tmp0[j+1] >> (2**j): tmp0[j+1];
  end
endgenerate

//assign pdn[PW] = (SUMp_rev==0)? 1'b0: 1'b1;
//assign pdn[PW-1:0] = SUMp_rev[0]? 5'h0: SUMp_rev[1]? 5'h1: SUMp_rev[2]? 5'h2: SUMp_rev[3]? 5'h3: SUMp_rev[4]? 5'h4: SUMp_rev[5]? 5'h5: SUMp_rev[6]? 5'h6: SUMp_rev[7]? 5'h7: 
				  //SUMp_rev[8]? 5'h8: SUMp_rev[9]? 5'h9: SUMp_rev[10]? 5'ha: SUMp_rev[11]? 5'hb: SUMp_rev[12]? 5'hc: SUMp_rev[13]? 5'hd: SUMp_rev[14]? 5'he: SUMp_rev[15]? 5'hf :5'hf;
				  //SUMp_rev[16]? 5'h10: SUMp_rev[17]? 5'h11: SUMp_rev[18]? 5'h12: SUMp_rev[19]? 5'h13: SUMp_rev[20]? 5'h14: SUMp_rev[21]? 5'h15: SUMp_rev[22]? 5'h16: SUMp_rev[23]? 5'h17:
				  //SUMp_rev[24]? 5'h18: SUMp_rev[25]? 5'h19: SUMp_rev[26]? 5'h1a: SUMp_rev[27]? 5'h1b: SUMp_rev[28]? 5'h1c: SUMp_rev[29]? 5'h1d: SUMp_rev[30]? 5'h1e: SUMp_rev[31]? 5'h1f: 5'h1f;
//DW01_binenc #(23+8+1,5+1) UFe ( .A(SUMp_rev), .ADDR(pdn));	
assign add     = !(D0s ^^ D1s);
assign sel_p1  = SUMm[m_wd+2] || SUMp[m_wd+2] && rnda1 || SUMmi[m_wd+2] && rnda2;
assign sel_0   = add && sel_p1? 1'b0: SUMm[m_wd+1] || SUMp[m_wd+1] && (rnds1 || !add && !(|diff_e[e_wd-1:1]) || sub_carry);	
assign sel_1   = (D0s ^^ D1s) && !sel_0 && (|DWe_0[e_wd-1:1]) && (SUMm[m_wd] || SUMp[m_wd] && !(|sh_sel[PW-1:1]));	
assign sel_pdn = !sel_p1 && !sel_0 && !sel_1;	

assign DWe_p1  = DWe_0 + 1'h1;
assign DWe_0   = D0e_lt_D1e? D1e_: D0e_;
assign DWe_1   = DWe_0 - 1'h1;
assign DWe_pdn = DWe_0 - pdn;	   	 

assign DWm_p1  = rnda2 || rnda1? SUMmi[m_wd+1:2]: SUMm[m_wd+1:2];
assign DWm_0   = rm0 && rnd0 && rem_carry1 && !add || rnd0 && (rm2 && !DWs || rm3 && DWs)? SUMp2[m_wd:1]: rnd0 || rem_carry1? SUMp[m_wd:1]: SUMm[m_wd:1];
assign DWm_1   = rnds1 || rem_carry1? SUMp[m_wd-1:0]: SUMm[m_wd-1:0];
assign DWm_pdn = |DWe_0[e_wd-1:1]? DWm_pdn_: SUMp[m_wd:1];

assign DWm_pdn_ = SUMp[m_wd-1:0] << shn;
//DW_shifter #(23,5) USn (.data_out(DWm_pdn_), .data_in(SUMp[23-1:0]), .sh(shn), .data_tc(1'b0), .sh_tc(1'b0), .sh_mode(1'b1));	 // left shift - normalization
assign shn = !UF? pdn - 1'h1: DWe_0[PW-1:0] - 2'h2;

assign rnda1 = D0_Z || D1_Z? 1'b0: rm0? SUMm[1] && (SUMm[2] || (SUMm[0] || rem_b2 || (|rem))): 0;
assign rnda2 = D0_Z || D1_Z? 1'b0: (rm2 && !DWs || rm3 && DWs) &&  SUMm[m_wd+2]? SUMm[1] || SUMm[0] || rem_b2 || (|rem) || add && (sh_sel > m_wd+1) : 
               (rm2 && !DWs || rm3 && DWs) && !SUMm[m_wd+2]? SUMm[1] &&(SUMm[0] || rem_b2 || (|rem) || add && (sh_sel > m_wd+1)): 0;	
assign rnd0  = rm0? (SUMm[0] || rem_carry1) && (SUMm[1] || (rem_b2 || (|rem)) && !rem_carry1):
               D0_Z || D1_Z? 1'b0: rm2 && !DWs || rm3 && DWs? SUMm[0] || rem_b2 || (|rem) || add && (sh_sel > m_wd+1): 0;	                
assign rnds1 = D0_Z || D1_Z? 1'b1: rm0? (rem_b2  || rem_carry2) && (SUMm[0] || (|rem) && !rem_carry2): 
               rm2 && !DWs || rm3 && DWs? rem_b2 || (|rem) || add && (sh_sel > m_wd+1): 0;	              		

// rounding
assign sh_sel = D0e_lt_D1e? sh0: sh1;
assign Dm_sel = D0e_lt_D1e? D0mi: D1mi;
//assign rem_b1 = SUMm[0];                // pushed bit 1	  
wire [e_wd+m_wd:0] tmp;
assign tmp = {{(e_wd-3){Dm_sel[m_wd+2]}},Dm_sel,Dm_sel[m_wd+2]};
assign rem_b2 = tmp[sh_sel];
//DW01_mux_any #(23+8+1,5,1) MXb2 ( .A({{(8-3){Dm_sel[23+2]}},Dm_sel,Dm_sel[23+2]}), .SEL(sh_sel), .MUX(rem_b2) ); // pushed bit 2	 
assign rem_msk = !rem_en? 0: {(e_wd+m_wd+1){1'b1}} >> (e_wd+m_wd-sh_rem);
//                 (sh_rem==5'h0)? 32'h1: (sh_rem==5'h1)? 32'h3: (sh_rem==5'h2)? 32'h7: (sh_rem==5'h3)? 32'hf: (sh_rem==5'h4)? 32'h1f: (sh_rem==5'h5)? 32'h3f: (sh_rem==5'h6)? 32'h7f: (sh_rem==5'h7)? 32'hff: 
//                 (sh_rem==5'h8) ? 32'h1ff: (sh_rem==5'h9 )? 32'h3ff: (sh_rem==5'ha )? 32'h7ff: (sh_rem==5'hb )? 32'hfff: (sh_rem==5'hc )? 32'h1fff: (sh_rem==5'hd )? 32'h3fff: (sh_rem==5'he )? 32'h7fff: (sh_rem==5'hf )? 32'hffff: 
//                 (sh_rem==5'h10)? 32'h1ffff: (sh_rem==5'h11)? 32'h3ffff: (sh_rem==5'h12)? 32'h7ffff: (sh_rem==5'h13)? 32'hfffff: (sh_rem==5'h14)? 32'h1fffff: (sh_rem==5'h15)? 32'h3fffff: (sh_rem==5'h16)? 32'h7fffff: (sh_rem==5'h17)? 32'hffffff: 
//                 (sh_rem==5'h18)? 32'h1ffffff: (sh_rem==5'h19)? 32'h3ffffff: (sh_rem==5'h1a)? 32'h7ffffff: (sh_rem==5'h1b)? 32'hfffffff: (sh_rem==5'h1c)? 32'h1fffffff: (sh_rem==5'h1d)? 32'h3fffffff: (sh_rem==5'h1e)? 32'h7fffffff: 32'hffffffff; 
//DW_thermdec  #(5) Urm ( .en(rem_en), .a(sh_rem), .b(rem_msk) );	  								  
assign rem_en = add && (sh_sel > m_wd+1) || !add && (sh_sel > m_wd+2) || (sh_sel < 2)? 1'b0: 1'b1; 
assign sh_rem = sh_sel - 2;	 
assign rem = rem_msk[m_wd+1:0] & Dm_sel[m_wd+1:0] | {(m_wd+2){(sh_sel > m_wd+1) && add}};		 // pushed bits 3 etc.
assign rem_carry2 = add || (sh_sel > m_wd+2) && (rm1 || rm2 && DWs || rm3 && !DWs)? 0: &(~rem_msk[m_wd+1:0] | rem_msk[m_wd+1:0] & Dm_sel[m_wd+1:0]);	
assign rem_carry1 = D0_Z || D1_Z? 1'b1: rem_carry2 && rem_b2 || !add && (sh_sel < 2);	
//assign rem_carry0 = rem_carry1 && SUMm[0];	  
assign sub_carry  = add || (sh_sel > m_wd+2)? 0: rem_carry2 && rem_b2;	 // carry bit when subtract

// Output	 	   
assign DW  = {DWs,DWe,DWm[m_wd-1:0]};
assign DWs = !DW_NaN && !DW_Z && (D0_lt_D1? D1s: D0s) || opp_sign_sum_eq_0 && rm3 || D0_Z && D1_Z && D0s && D1s; 
assign DWe = OVF && Sat && sat_en? {{e_wd-1{1'b1}},1'b0}:
             DW_NaN || DW_inf || OVF || DW_Z || DW_dn? {e_wd{DW_NaN || DW_inf || OVF}}:
             {e_wd{sel_p1}} & DWe_p1 |
			 {e_wd{sel_0 }} & DWe_0  |
			 {e_wd{sel_1 }} & DWe_1  | 
			 {e_wd{sel_pdn}}& DWe_pdn;
assign DWm = DW_NaN || DW_inf || OVF || DW_Z? {m_wd{DW_NaN || OVF && Sat && sat_en}}:
             {m_wd{sel_p1}} & DWm_p1 |
			 {m_wd{sel_0 }} & DWm_0  |
			 {m_wd{sel_1 }} & DWm_1  | 
			 {m_wd{sel_pdn}}& DWm_pdn;
 
 //  overflow & underflow  & denormalized
assign OVF = add && sel_p1 && (DWe_0=={{e_wd-1{1'b1}},1'b0});			  // overflow
assign UF  = sel_1  && (DWe_0=={{e_wd-1{1'b0}},1'b1}) || sel_pdn && !(|DWe_0[e_wd-1:PW]) && (DWe_0[PW-1:0] <= pdn[PW-1:0]);  // underflow
assign DW_dn = (D0_dn || D0_Z) && (D1_dn || D1_Z) && !sel_0 && !DW_Z || UF && !DW_Z;	 // Output denormalized	
assign sat_en = OVF && (rm1 || rm2 && DWs || rm3 && !DWs) && !DW_NaN && !DW_inf;

endmodule  