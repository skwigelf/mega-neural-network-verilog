module matrix_to_flat_array
	#(parameter NUMBER_SIZE = 15,
	  parameter MATRIX_SIZE = 6)(
	input [NUMBER_SIZE:0] matrix [MATRIX_SIZE - 1:0][MATRIX_SIZE - 1:0], 
	output [NUMBER_SIZE:0] array [MATRIX_SIZE * MATRIX_SIZE - 1:0]	
);
	genvar i;
	genvar j;
	generate
		begin
			for (i = 0; i < MATRIX_SIZE; i = i + 1) begin : y_count
				for (j = 0; j < MATRIX_SIZE; j = j + 1) begin : x_count
					assign array[i * MATRIX_SIZE + j] = matrix[i][j];
				end
			end
		end
	endgenerate
endmodule