module flat_to_array_160
	#(parameter NUMBER_SIZE = 16,
	  parameter KERNEL_SIZE = 12)
	(
	input [KERNEL_SIZE * NUMBER_SIZE - 1:0] value,
	output [NUMBER_SIZE:0] matrix [KERNEL_SIZE - 1:0]
	);	
	
	genvar i;
	generate
	begin
		for (i = 0; i <	KERNEL_SIZE; i = i + 1)
			begin : flatter
				assign matrix[i] = value[(i + 1) * NUMBER_SIZE - 1 : i * NUMBER_SIZE];
			end
	end
	endgenerate
endmodule